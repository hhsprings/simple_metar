# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
from os import path
import httplib2  # https://pypi.python.org/pypi/httplib2
from datetime import datetime
import pickle
try:
    from cStringIO import StringIO  # CPython 2.7
except ImportError:
    from StringIO import StringIO  # Python 3.x or not CPython
# pytree exists in https://code.google.com/archive/p/pyrtree/,
# i.e., that is slightly old (project is dead), but it is written
# as pure Python, so it is proper for me. (Though it doesn't have
# nearest neighbour lookup, but it's not difficult to write it.)
from pyrtree import Rect
from .config import config

# --- module globals
#
_BASE_URL = "http://tgftp.nws.noaa.gov/data/observations/metar"

#
_knwon_st_serialized_fn = path.join(
    path.dirname(path.abspath(__file__)),
    "data",
    "knownloc_data.pickle")
_knwon_st_raw = pickle.load(open(_knwon_st_serialized_fn, "rb"))
_KNOWN_STATIONS_IDX = _knwon_st_raw["index"]
_KNOWN_STATIONS = _knwon_st_raw["infos"]
del _knwon_st_serialized_fn
del _knwon_st_raw
#
# ---


# --- module helpers
#
def _split_contents(contents):
    dts = None
    for line in contents:
        line = line.strip()
        if not line:
            continue
        if not dts:
            dts = line
        else:
            yield (dts, line)
            dts = None


def _urlopener():
    return httplib2.Http(config.get_cache_dir("noaa_services"))


def _build_headers():
    return {"Cache-Control": config.http_cachecontrol}
#
# ---


# --- public interfaces
#
def get_current_by_single_station(station_cd):
    """
    retrieve METAR observations by station_cd. station_cd is ICAO 4-letter airport code.
    yield (datetime string, metar code) tuples.
    """
    url = "{}/stations/{}.TXT".format(_BASE_URL, station_cd)
    headers, contents = _urlopener().request(url, "GET", headers=_build_headers())
    #yield from _split_contents(contents)
    for r in _split_contents(StringIO(contents)):
        yield r


def get_single_cycle(cycle):
    """
    retrieve METAR observations by cycle. cycle is hour string (00 - 23).
    yield (datetime string, metar code) tuples.
    """
    url = "{}/cycles/{:02d}Z.TXT".format(_BASE_URL, int(cycle))
    headers, contents = _urlopener().request(url, "GET", headers=_build_headers())

    # cycles have duplicating records...we want to:
    # ('2016/08/24 09:00', 'RJTT 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG RMK 1CU020 3AC140 A2976') -> take
    # ('2016/08/24 09:00', 'RJTT 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG RMK 1CU020 3AC140 A2976') -> omit
    # ('2016/08/24 09:00', 'RJTT 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG') -> omit
    # ('2016/08/24 09:00', 'RJTT 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG') -> omit
    #
    # ('2016/08/24 09:00', 'RJAA 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG') -> take
    # ('2016/08/24 09:00', 'RJAA 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG') -> omit
    # ('2016/08/24 09:00', 'RJAA 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG RMK 1CU020 3AC140 A2976') -> take
    # ('2016/08/24 09:00', 'RJAA 240900Z 20011KT 9999 FEW020 SCT140 28/24 Q1007 NOSIG RMK 1CU020 3AC140 A2976') -> omit
    dejav = set()
    for dt, code in _split_contents(StringIO(contents)):
        spl = code.partition(" RMK ")
        if (dt, code) not in dejav:
            dejav.add((dt, spl[0]))
            if spl[1]:
                dejav.add((dt, code))
            yield (dt, code)


def get_current_cycle():
    """
    retrieve METAR current cycle observations.
    yield (datetime string, metar code) tuples.
    """
    now = datetime.utcnow()

    # deciding cycle logic from
    #     https://github.com/phobson/python-metar/blob/master/metar/metar.py#L619
    if now.minute < 45:
        cycle = now.hour
    else:
        cycle = now.hour + 1
    
    for r in get_single_cycle(cycle % 24):
        yield r


def get_current_all_cycles():
    """
    retrieve METAR current observations of all cycles.
    yield (datetime string, metar code) tuples.
    """
    now = datetime.utcnow()
    if now.minute < 45:
        last_cycle = now.hour
    else:
        last_cycle = now.hour + 1

    dejav = set()
    for cycle in [i if i > 0 else i + 24 for i in range(last_cycle, last_cycle - 24, -1)]:
        for dt, code in get_single_cycle(cycle % 24):
            spl = code.partition(" RMK ")
            if (dt, code) not in dejav:
                dejav.add((dt, spl[0]))
                if spl[1]:
                    dejav.add((dt, code))
                yield (dt, code)


def search_stations_by_boundingbox(left, bottom, right, top):
    """
    search knwon stations serving METAR.

    left: i.e, min longitude
    bottom: i.e, min latitude
    right: i.e, max longitude
    top: i.e, max latitude
    """
    # There are left > right pattens: for example, 178 ~ 182 (i.e, -178.).
    if left > right:
        target_rects = [
            Rect(left, bottom, 180, top),
            Rect(-180, bottom, right, top),
            ]
    else:
        target_rects = [
            Rect(left, bottom, right, top),
            ]
    for target_rect in target_rects:
        for r in _KNOWN_STATIONS_IDX.query_rect(target_rect):
            if r.is_leaf():
                station_id = r.leaf_obj()
                name, lon, lat = infos = _KNOWN_STATIONS[station_id]  # name, lon, lat
                if target_rect.does_containpoint([lon, lat]):
                    yield (station_id, infos)


def search_stations_by_stationid(station_id):
    return _KNOWN_STATIONS.get(station_id)
#
# ---
