# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
import re
from .common import _Base, _General, _ColourState, _to_fracint
from ..abbreviations import CLOUD_TYPES, WEATHER_CODES_OBSCURATION


# QFE in Russian convention
class _QFE_Russian(_General):
    def __init__(self):
        super(_QFE_Russian, self).__init__(
            re.compile(r"^QFE(?P<qfe_mmhg>\d\d\d)(/(?P<qfe_hpa>\d\d\d\d))?\s+"), False)

    def _reveal_groupdict(self, g):
        g["qfe_mmhg"] = int(g["qfe_mmhg"])
        if "qfe_hpa" in g:
            g["qfe_hpa"] = int(g["qfe_hpa"])
        return g

    def convention(self, countries):
        if "RUSSIA" in countries:
            return "using"
        return "not using"


# Amount of accumulated precipitation since the previous METAR (PPrrr) (Peru's convention)
#
# For example:
#   PP000 means 0.0 millimeter
#   PP023 means 2.3 millimeter
#   PP300 means 30.0 millimeter
#
# TNX: https://www.reddit.com/r/flying/comments/4vi5xp/metar_headscratcher/
#
class _AmountOfAccumulatedPrecipitationSinceThePrevious_Peru(_General):
    def __init__(self):
        super(_AmountOfAccumulatedPrecipitationSinceThePrevious_Peru, self).__init__(
            re.compile(r"^PP(?P<precipitation>\d\d\d)\s+"), False)

    def _reveal_groupdict(self, g):
        g["precipitation"] = int(g["precipitation"]) / 10.
        g["unit"] = "millimeter"
        return g

    def convention(self, countries):
        if "PERU" in countries:
            return "using"
        return "not using"


#
# Bird Hazard (BIRD HAZARD RWY rr[/rr]) (Peru's convention)
#
#     In Peru, it is likely to report a bird hazard (bird-strike) in METAR RMK.
#
class _BirdHazard_Peru(_General):
    def __init__(self):
        super(_BirdHazard_Peru, self).__init__(
            re.compile(r"^BIRD\sHAZARD\sRWY\s(?P<runways>(\d\d[LRC]*)(/(\d\d[LRC]*))*)\s+"), False)

    def _reveal_groupdict(self, g):
        g["runway"] = g.pop("runways").split("/")
        return g

    def convention(self, countries):
        if "PERU" in countries:
            return "using"
        return "not using"


#
# Cloud cover in Japan convention (NsCCHsHsHs)
#   Ns: cloud cover (1 - 8 or /)
#   CC: cloud type (AC, AS, NS, SC, ST, CU, CB, TCU, //)
#   HsHsHs: cloud base altitude in handred feets
#
class _CloudCover_Japan(_General):

    def __init__(self):
        # The "航空気象通報式 第３版" doesn't mention to report CI (Cirrus),
        # but actually is reporting.
        cloud_forms = "|".join(CLOUD_TYPES.keys())
        super(_CloudCover_Japan, self).__init__(
            re.compile(r"""^
                       (?P<cover>[1-8/])
                       (?P<form>({cloud_forms}|//))
                       (?P<altitude>\d\d\d)
                       \s+""".format(cloud_forms=cloud_forms),
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if g["cover"] != "/":
            g["cover"] = g["cover"] + " oktas"
        g["altitude"] = int(g["altitude"])
        g["altitude_unit"] = "100ft"
        return g

    def convention(self, countries):
        if "JAPAN" in countries:
            return "using"
        return "not using"


#
# Cloud cover in Canada convention (CCNsCCNs...)
#   CC: cloud type or obscuring phenomena
#   Ns: cloud cover (1 - 8)
#
class _CloudCover_Canada(_Base):

    def __init__(self):
        super(_CloudCover_Canada, self).__init__(True)
        cloud_forms = CLOUD_TYPES.keys()
        obscuration = WEATHER_CODES_OBSCURATION.keys()
        self._rgx = re.compile(
            r"((?P<cloud_form>{})|(?P<obscuration>{}))(?P<cover>[1-8])(?P<reject>/?)".format(
                "|".join(cloud_forms), "|".join(obscuration)))

    def convention(self, countries):
        if "CANADA" in countries:
            return "using"
        return "not using"

    def match(self, code):
        orig_code = code

        gall = []

        result = []
        m = self._rgx.match(code)
        while m:
            if m.group("reject"):
                break
            gall.append(m.group())

            d = {"cover": "{} oktas".format(m.group("cover"))}
            if m.group("cloud_form"):
                d["cloud_form"] = m.group("cloud_form")
            if m.group("obscuration"):
                d["obscuration"] = m.group("obscuration")
            result.append(d)
            code = code[m.end():]
            m = self._rgx.match(code)

        if not gall or (m and m.group("reject")):
            return [None, None, orig_code]

        if code.startswith(" "):
            code = code[1:]

        return ["".join(gall).rstrip(), {"phenomenons": result}, code]
