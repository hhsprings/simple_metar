# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
import re
from fractions import Fraction


def _compactdict(d):
    return {k: v for k, v in d.items() if v is not None and v != ''}

#
class _Base(object):
    def __init__(self, repeatable=True):
        self.repeatable = repeatable

    def _reveal_groupdict(self, g):
        return g

    def convension(self, countries):
        return "using"


#
class _General(_Base):
    def __init__(self, rgx, repeatable=True):
        super(_General, self).__init__(repeatable)
        self._rgx = rgx

    def match(self, code):
        m = self._rgx.match(code)
        if m:
            return [
                m.group().rstrip(),
                self._reveal_groupdict(_compactdict(m.groupdict())),
                code[m.end():]
                ]
        return [None, None, code]


#
# Wind Group
#     not in RMK: (dddff(f)Gfmfm(fm)Kt_dndndnVdxdxdx)
#     occurs in RMK in various format:
#         WIND 412FT 19005KT 110V230 - DENMARK, etc.
#         RWY11 29004KT 240V340 - CYPRUS, etc.
#         WIND THR03 19003G13KT WIND THR021 08005KT - ITALY, etc.
#         RWY21L 01010KT 340V070 RWY03L 03009KT 340V070 RWY21R 03009KT - TURKEY, etc.
#         HARBOR WIND 28016G21KT - USA, etc.
#         R09/VRB05G15KT ARP/VRB05G17KT	- SPAIN, etc.
#
class _Wind(_General):
    def __init__(self):
        # Both WMO 306 and NOAA FH-1 1995 don't mention those
        # missing indicator, but actually /// exists. (but i have not ever seen MMM.)
        super(_Wind, self).__init__(
            re.compile(r"""^
                     (
                     # occuring only RMK section.
                     (?P<kind>(HARBOR\s)?(WIND\s))?
                     ((?P<altitude>\d+)(?P<altitude_unit>FT)\s)?
                     ((R(WY)?(?P<runway>(0?\d\d[LRC]*))|THR(?P<runway_threshold>(0?\d\d[LRC]*))|(?P<special_position>ARP))[\s/])?

                     # standard part
                     ((?P<direction>\d{3})|(?P<dir_missing_or_vrb>(///|MMM|VRB)))
                     ((?P<speed_greaterthan>P)?(?P<speed>\d{2,3})|(?P<speed_missing>[/M]{2,3}))
                   (G((?P<gust_greaterthan>P)?(?P<gust>\d{1,3})|(?P<gust_missing>[/M]{1,3})))?
                      (?P<units>KTS?|LT|K|T|KMH|MPS)
                  (\s+(?P<direction_var_from>\d\d\d)V
                      (?P<direction_var_to>\d\d\d))?)\s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        if "dir_missing_or_vrb" in g:
            g["direction"] = g.pop("dir_missing_or_vrb")
        elif "direction" in g:
            g["direction"] = int(g["direction"])
        if "speed_missing" in g:
            g["speed"] = g.pop("speed_missing")
        elif "speed" in g:
            g["speed"] = int(g["speed"])
        if "gust_missing" in g:
            g["gust"] = g.pop("gust_missing")
        elif "gust" in g:
            g["gust"] = int(g["gust"])
        if "speed_greaterthan" in g:
            g["speed_greaterthan"] = True
        if "gust_greaterthan" in g:
            g["gust_greaterthan"] = True
        if "direction_var_from" in g:
            g["direction_var_from"] = int(g["direction_var_from"])
        if "direction_var_to" in g:
            g["direction_var_to"] = int(g["direction_var_to"])
        if "altitude" in g:
            g["altitude"] = int(g["altitude"])
        return g


#
# countries using WMO standard, place altimeter setting code in hectopascals first,
# then in remarks section code in hundredths of inches of mercury.
#
# ex. RJAA 250000Z 14005KT 120V180 9999 FEW015 28/23 Q1009 NOSIG RMK 2CU015 A2982
#
class _Qnh(_General):
    def __init__(self):
        super(_Qnh, self).__init__(
            re.compile(r"""^(?P<setting_type>A|Q)(?P<pressure>\d{4}|////)\s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        st = g.pop('setting_type')
        if st == 'Q':  # international (i.e. in WMO)
            g['unit'] = 'hPa'  # or MB
            if g['pressure'] != '////':
                g['pressure'] = int(g['pressure'])
        else:
            g['unit'] = 'in Hg'
            if g['pressure'] != '////':
                g['pressure'] = int(g['pressure']) / 100.
        return g


class _ColourState(_General):
    """
    Meanings of the colour codes depends on context, mainly reporting
    weather station, especially which the station is civil or military.

    If an airfield runway is unusable for reasons other than clouds or
    low visibility (e.g. ice or other obstructions) then the word BLACK
    is written in full and placed immediately before the actual colour
    state (e.g. BLACKWHT).
    """
    def __init__(self):
        _colors = ("BLU", "WHT", "GRN", "YLO", "YLO1", "YLO2", "AMB", "RED")
        _s = r"(BLACK)?({})\+?".format("|".join(_colors))
        super(_ColourState, self).__init__(
            re.compile(r"^({s}(/{s})*)(\s({s}(/{s})*))*\s+".format(s=_s)))


#
class _LocMov(_Base):
    def __init__(self, repeatable=True):
        super(_LocMov, self).__init__(repeatable)

        # 1ST 2ND 3RD 3HD 3TD 4TH
        # 3HD, 3TD -> maybe mispell of 3RD
        self._quads_rgx = re.compile(r"""^
            (?P<quads>(
               (
                   ALL\s
                   |
                   (([/-]|AND\s)?\s?([1-4]([SNRHT][TDH])?\s?|[NSEW][EW]*\s))*
               )
               QUADS?
               (\sEXCEPT\s[NSEW][EW]*)?
               )
            )\s+""", re.VERBOSE)

        c1 = r"([-+/]?\s?(VC|DSIPT|ALQD?S|OHD|\d*[NSEW][EW]*|ALQD?S|STNRY?)?)+"

        self._loc_rgx = re.compile(r"""^
            (?P<loc>(
                (LOC\s?)?
                ((VC|OHD)\s?)?
                ((DSNT|DI?ST(ANT)?)\s?)?
                (APRX\s)?(\d*\s?(KM|NM|SM)\s)?{c1}
                (
                    (\s(AND|THRU|&|/))?
                    (\s(DSNT|DI?ST))?
                    \s{c1}
                )*
            ))\s+""".format(c1=c1), re.VERBOSE)

        self._mov_rgx = re.compile(r"""^
            (
                (?P<move_type>
                    ((MO?V[DG]?|DRFT[DG]?|TO))
                    (\s(SLO(WLY)?|LTL))?
                )
                (\s(?P<move_to>{c1}))?
            )\s+""".format(c1=c1), re.VERBOSE)

    def match(self, code):
        gall = []
        rvldict = {}

        for rgx in (self._quads_rgx, self._loc_rgx, self._mov_rgx):
            m = rgx.match(code)
            if m:
                code = code[m.end():]
                gall.append(m.group())
                rvldict.update(self._reveal_groupdict(_compactdict(m.groupdict())))

        if not gall:
            return [None, None, code]

        return ["".join(gall).rstrip(), rvldict, code]


def _to_fracint(rawstr):
    res = sum([Fraction(s) for s in rawstr.split(" ")])
    if res.denominator == 1:
        res = res.numerator
    return res
