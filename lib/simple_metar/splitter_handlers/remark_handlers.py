# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
import re
from .common import (
    _compactdict,
    _General, _Wind, _Qnh, _ColourState, _Base, _LocMov,
    _to_fracint
    )
from .national_conventions import (
    _QFE_Russian,
    _AmountOfAccumulatedPrecipitationSinceThePrevious_Peru,
    _BirdHazard_Peru,
    _CloudCover_Japan,
    _CloudCover_Canada,
    )
from ..abbreviations import (
    WEATHER_CODES_OBSCURATION,
    CLOUD_TYPES
    )


# Peak Wind (PK_WND_dddff(f)/(hh)mm)
class _PeakWind(_General):
    def __init__(self):
        super(_PeakWind, self).__init__(
            re.compile(r"""^PK\s+WND\s+
                               (?P<direction>\d\d\d)
                               (?P<speed>P?\d\d\d?)/
                               (?P<hour>\d\d)?
                               (?P<minute>\d\d)\s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        return {k: int(g[k]) for k in g}


# Wind Shift (WSHFT_(hh)mm)
class _WindShift(_General):
    def __init__(self):
        super(_WindShift, self).__init__(
            re.compile(r"""^WSHFT\s+
                                (?P<hour>\d\d)?
                                (?P<minute>\d\d)
                                (\s+(?P<front>FROPA))?\s+""",
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "hour" in g:
            g["hour"] = int(g["hour"])
        g["minute"] = int(g["minute"])
        return g


# Tower or Surface Visibility (TWR_VIS_vvvvv or SFC_VIS_vvvvv)
class _TowerOrSurfaceVisibility(_General):
    def __init__(self, twr_srf):
        super(_TowerOrSurfaceVisibility, self).__init__(
            re.compile(r"""^
              {}\sVIS\s+
              (?P<visibility>(\d+\s+)?\d+(/\d+)?)
              \s+
              """.format(twr_srf), re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        g["visibility"] = _to_fracint(g["visibility"])
        return g


# Variable Prevailing Visibility (VIS_vnvn vnvnVvxvxvx vxvx)
class _VariablePrevailingVisibility(_General):
    def __init__(self):
        super(_VariablePrevailingVisibility, self).__init__(
            # standard: "VIS_vnvn vnvnVvxvxvx vxvx"
            # rare:     "VIS_VRB_vnvn vnvn-vxvxvx vxvx"
            re.compile(r"""^
          VIS\s+
          (VRB\s)?(?P<lowest>(\d+\s+)?\d+(/\d+)?)
          (V|-)(?P<highest>(\d+\s+)?\d+(/\d+)?)
          \s+
              """, re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        g["lowest"] = _to_fracint(g["lowest"])
        g["highest"] = _to_fracint(g["highest"])
        return g


# Lightning (Frequency_LTG(type)_[LOC])
# Thunderstorm Location (TS_LOC_(MOV_DIR)) [Plain Language]
# Significant Cloud Types [Plain Language]
#    TODO: check BLDU... (maybe it is cloud type or growing of CB, etc)
class _Lightning(_Base):
    def __init__(self):
        super(_Lightning, self).__init__(True)
        self._rgx = re.compile(r"""^
                 ((?P<lightning_freq>OCNL|FRE?Q|CONS|FQT)\s+)?
                 (
                     ((LTN?G)\s?((?P<lightning_type>(IC|CC|CG|CA)*)?))
                     |
                     (((?P<lightning_type_2>(IC|CC|CG|CA)*)?)\s?(LTN?G))
                 )
                 \s+""", re.VERBOSE)
        self._locmov = _LocMov()

    def _reveal_groupdict(self, g):
        s = ""
        if "lightning_type" in g:
            s = g.pop("lightning_type")
        elif "lightning_type_2" in g:
            s = g.pop("lightning_type_2")
        if s:
            g["lightning_types"] = [s[i:i+2] for i in range(0, len(s), 2)]
        return g

    def match(self, code):
        m1 = self._rgx.match(code)
        if not m1:
            return [None, None, code]

        gall = []
        rvldict = {}

        code = code[m1.end():]
        gall.append(m1.group())
        rvldict.update(self._reveal_groupdict(_compactdict(m1.groupdict())))
        gall_locmov, rvldict_locmov, code = self._locmov.match(code)
        if gall_locmov:
            gall.append(gall_locmov)
            rvldict.update(rvldict_locmov)
        return ["".join(gall).rstrip(), rvldict, code]


class _ThunderstormLocation(_Base):
    def __init__(self):
        super(_ThunderstormLocation, self).__init__(True)
        self._rgx = re.compile(r"""^TS\s+""", re.VERBOSE)
        self._locmov = _LocMov()


    def match(self, code):
        m1 = self._rgx.match(code)
        if not m1:
            return [None, None, code]

        gall = []
        rvldict = {}

        code = code[m1.end():]
        gall.append(m1.group())
        rvldict.update(self._reveal_groupdict(_compactdict(m1.groupdict())))
        gall_locmov, rvldict_locmov, code = self._locmov.match(code)
        if gall_locmov:
            gall.append(gall_locmov)
            rvldict.update(rvldict_locmov)
        return ["".join(gall).rstrip(), rvldict, code]


class _SignificantCloudTypes(_Base):
    def __init__(self):
        super(_SignificantCloudTypes, self).__init__(True)
        self._rgx = re.compile(r"""^
                 (?P<cloud>(
                   (\sAND\s)?((BLDU|CBS?|CBMAM|TCU|ACC|(APRNT\sROTOR\s)?(CLD|SCSL|ACSL|CCSL))))+
                 )\s+""", re.VERBOSE)
        self._locmov = _LocMov()

    def match(self, code):
        m1 = self._rgx.match(code)
        if not m1:
            return [None, None, code]

        gall = []
        rvldict = {}

        code = code[m1.end():]
        gall.append(m1.group())
        rvldict.update(self._reveal_groupdict(_compactdict(m1.groupdict())))
        gall_locmov, rvldict_locmov, code = self._locmov.match(code)
        if gall_locmov:
            gall.append(gall_locmov)
            rvldict.update(rvldict_locmov)
        return ["".join(gall).rstrip(), rvldict, code]


# Beginning and Ending of Precipitation (w'w'B(hh)mmE(hh)mm)
#     Notice(1): multiple reports is concatinated without space
#                like "SHRAB05E30SHSNB20E55".
#     Notice(2): begin and end are both optional and in random order
#                like "RAE2259B45"
class _BeginningAndEndingOfPrecipitation(_General):
    def __init__(self):
        super(_BeginningAndEndingOfPrecipitation, self).__init__(
            re.compile("""^
        (?P<descriptor>MI|PR|BC|DR|BL|SH|TS|FZ)?
        (?P<precipitation>(DZ|RA|SN|SG|IC|PL|GR|GS|UP)+)
        (?P<begin_or_end_1>[BE])(?P<hour1>(\d\d|MM))?(?P<minute1>(\d\d|MM))
        # endless? i found "RAB02E36B53"
        ((?P<begin_or_end_2>[EB])(?P<hour2>(\d\d|MM))?(?P<minute2>(\d\d|MM)))?
        ((?P<begin_or_end_3>[EB])(?P<hour3>(\d\d|MM))?(?P<minute3>(\d\d|MM)))?
        ((?P<begin_or_end_4>[EB])(?P<hour4>(\d\d|MM))?(?P<minute4>(\d\d|MM)))?
        \s*
        """, re.VERBOSE), True)


# Beginning and Ending of Thunderstorms (TSB(hh)mmE(hh)mm)
#     Notices are the same as `Beginning and Ending of Precipitation'.
class _BeginningAndEndingOfThunderstorms(_General):
    def __init__(self):
        super(_BeginningAndEndingOfThunderstorms, self).__init__(
            re.compile("""^
        TS
        (?P<begin_or_end_1>[BE])(?P<hour1>(\d\d|MM))?(?P<minute1>(\d\d|MM))
        # endless? i found "RAB02E36B53"
        ((?P<begin_or_end_2>[EB])(?P<hour2>(\d\d|MM))?(?P<minute2>(\d\d|MM)))?
        ((?P<begin_or_end_3>[EB])(?P<hour3>(\d\d|MM))?(?P<minute3>(\d\d|MM)))?
        ((?P<begin_or_end_4>[EB])(?P<hour4>(\d\d|MM))?(?P<minute4>(\d\d|MM)))?
        \s*
        """, re.VERBOSE), True)


# Variable Ceiling Height (CIG_hnhn hnVhxhxhx)
class _VariableCeilingHeight(_General):
    def __init__(self):
        super(_VariableCeilingHeight, self).__init__(
            re.compile(r"^CIG\s+(?P<lowest>\d+)V(?P<highest>\d+)\s+"), False)

    def _reveal_groupdict(self, g):
        g["lowest"] = int(g["lowest"])
        g["highest"] = int(g["highest"])
        g["units"] = "100ft"
        return g


# Obscurations (w'w'_[NsNsNs] hshshs) [Plain Language]
class _Obscurations(_General):
    def __init__(self):
        super(_Obscurations, self).__init__(
            re.compile(r"""^
                (?P<obscuration>{})\s
                (?P<cover>BKN|SCT|FEW|[O0]VC)\s?
                (?P<height>\d{{3}}|///)
                \s+""".format("|".join(WEATHER_CODES_OBSCURATION.keys())), re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "height" in g and g["height"] != '///':
            g["height"] = int(g["height"])
            g["height_unit"] = "100ft"
        return g


# Variable Sky Condition (NsNsNs (hshshs)_V_ NsNsNs) [Plain Language]
class _VariableSkyCondition(_General):
    def __init__(self):
        super(_VariableSkyCondition, self).__init__(
            re.compile(r"""^
                        (?P<cover_varying1>VV|CLR|SKC|SCK|NSC|NCD|BKN|SCT|FEW|[O0]VC)
                        (?P<height>\d{3}|///)?
                        \sV\s
                        (?P<cover_varying2>VV|CLR|SKC|SCK|NSC|NCD|BKN|SCT|FEW|[O0]VC)
                        \s+""",
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "height" in g and g["height"] != '///':
            g["height"] = int(g["height"])
            g["height_unit"] = "100ft"
        return g


# Sea-Level Pressure (SLPppp)
class _SeaLevelPressure(_General):
    def __init__(self):
        super(_SeaLevelPressure, self).__init__(
            re.compile(r"^^SLP(?P<pressure>(\d\d\d|///))\s+"), False)

    def _reveal_groupdict(self, g):
        if g["pressure"] != "///":
            # General rule: add 9 if number is between 500 and 999
            #               add 10 if number is between 000 and 499
            if g["pressure"][0] in ("0", "1", "2", "3", "4"):
                # > 1000hPa (actually, < 1100hPa at sea-level)
                #   1010.2 -> SLP102
                g["pressure (calculated in general rule)"] = int("10" + g["pressure"]) / 10.
            else:
                # < 1000hPa (actually, > 900hPa at sea-level)
                #   998.2 -> SLP982
                g["pressure (calculated in general rule)"] = int("9" + g["pressure"]) / 10.

            # in extreme circumstances (for example hurricanes),
            # general rule would be wrong.
            g["pressure (raw value)"] = int(g.pop("pressure")) / 10.
        g["unit"] = "hPa"
        return g


# Hourly Precipitation Amount (Prrrr)
# 3- and 6-hour Precipitation (6RRRR)
# 24-Hour Precipitation Amount (7R24R24R24R24)
class _PrecipitationAmount(_General):
    def __init__(self, hrtype):
        super(_PrecipitationAmount, self).__init__(
            re.compile(r"^{}(?P<precipitation>(\d\d\d\d|////))\s+".format(hrtype)),
            False)

    def _reveal_groupdict(self, g):
        if g["precipitation"] != '////':
            g["precipitation"] = int(g["precipitation"]) / 100.
            g["unit"] = 'inch'
        return g


# Snow Depth on Ground (4/sss)
class _SnowDepthOnGround(_General):
    def __init__(self):
        super(_SnowDepthOnGround, self).__init__(
            re.compile(r"^4/(?P<total_snow_depth>(\d\d\d))\s+"), False)

    def _reveal_groupdict(self, g):
        g["total_snow_depth"] = int(g["total_snow_depth"])
        g["unit"] = 'inch'
        return g


# Water Equivalent of Snow on Ground (933RRR)
class _WaterEquivalentOfSnowOnGround(_General):
    def __init__(self):
        super(_WaterEquivalentOfSnowOnGround, self).__init__(
            re.compile(r"^933(?P<water_equivalent_of_snow>(\d\d\d))\s+"),
            False)

    def _reveal_groupdict(self, g):
        g["water_equivalent_of_snow"] = int(g["water_equivalent_of_snow"]) / 10.
        g["unit"] = 'inch'
        return g


# Duration of Sunshine (98mmm)
class _DurationOfSunshine(_General):
    def __init__(self):
        super(_DurationOfSunshine, self).__init__(
            re.compile(r"^98(?P<minutes>(\d\d\d))\s+"), False)

    def _reveal_groupdict(self, g):
        g["minutes"] = int(g["minutes"])
        return g


# Hourly Temperature and Dew Point (TsnT'T'T'snT'dT'dT'd)
class _HourlyTemperatureAndDewPoint(_General):
    def __init__(self):
        super(_HourlyTemperatureAndDewPoint, self).__init__(
            re.compile(r"""^T(?P<tsign>0|1)
                           (?P<temperature>\d\d\d)
                           ((?P<dsign>0|1)
                           (?P<dewpoint>\d\d\d))?\s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        g["temperature"] = int(g["temperature"]) / 10. * (-1 if g.pop("tsign") == 1 else 1)
        if "dewpoint" in g:
            g["dewpoint"] = int(g["dewpoint"]) / 10. * (-1 if g.pop("dsign") == 1 else 1)
        return g


# 6-Hourly Maximum Temperature (1snTx TxTx) / 6-Hourly Minimum Temperature (2snTx TxTx)
class _SixHourlyMaximumMinimumTemperature(_General):
    def __init__(self, minmax):
        super(_SixHourlyMaximumMinimumTemperature, self).__init__(
            re.compile(r"""^{}
                              (?P<sign>0|1)
                              (?P<temperature>\d\d\d)\s+""".format(minmax),
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        g["temperature"] = int(g["temperature"]) / 10. * (-1 if g.pop("sign") == 1 else 1)
        return g


# 24-hour Maximum and Minimum Temperature (4sn TxTxTxsnTn TnTn)
class _TwentyfourHourMaximumMinimumTemperature(_General):
    def __init__(self):
        super(_TwentyfourHourMaximumMinimumTemperature, self).__init__(
            re.compile(r"""^4
                              (?P<maxsign>0|1)
                              (?P<max_temperature>\d\d\d)
                              (?P<minsign>0|1)
                              (?P<min_temperature>\d\d\d)
                              \s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        g["max_temperature"] = int(g["max_temperature"]) / 10. * (-1 if g.pop("maxsign") == 1 else 1)
        g["min_temperature"] = int(g["min_temperature"]) / 10. * (-1 if g.pop("minsign") == 1 else 1)
        return g


# 3-Hourly Pressure Tendency (5appp)
class _ThreeHourlyPressureTendency(_General):
    def __init__(self):
        super(_ThreeHourlyPressureTendency, self).__init__(
            re.compile(r"""^5(?P<tendency>[0-8/])
                           (?P<barometric_change>(\d\d\d|///))\s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        if g["barometric_change"] != '///':
            g["barometric_change"] = int(g["barometric_change"]) / 10.
        g["unit"] = 'hPa'
        return g


# Sensor Status Indicators
class _SensorStatus(_General):
    def __init__(self):
        super(_SensorStatus, self).__init__(
            re.compile(r"""^(
                       (?P<type>(RVRNO|PWINO|PNO|FZRANO|TSNO|SLPNO))
                       |
                       ((?P<type2>(VISNO|CHINO))(\s(?P<loc>[^\s]+))?)
                       )
                       \s+""",
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "type2" in g:
            g["type"] = g.pop("type2")
        return g


# Density Altitude (DENSITY ALT hhhhFT)
class _DensityAltitude(_General):
    def __init__(self):
        super(_DensityAltitude, self).__init__(
            re.compile(r"^DENSITY ALT (?P<altitude>\d{3,5})(?P<unit>FT)\s+"), False)

    def _reveal_groupdict(self, g):
        g["altitude"] = int(g["altitude"])
        return g


REMARKINFOS = [
    # RMKs by national decision which shall not be disseminated internationally.

    # Sensor Status Indicators
    ("Sensor Status Indicators", _SensorStatus()),
    # Missing
    #   be able to Sensor Status Indicators?
    ("Missing or Not Available",
     _General(re.compile(r"""^
            (?P<info>(RVR|CLD|ICG|T|TD|WND|CB|TS|TS/LTNG|WX|VIS|PCPN|PRES))\s
            (?P<missing>MISG|INFO\sNOT\sAVBL|TEMPO\sUNAVBL)
            \s+""", re.VERBOSE), True)),

    # TODO: Volcanic Eruptions (Plain Language) -> can't parse

    # TODO: Funnel Cloud (Tornadic activity_B/E(hh)mm_LOC/DIR_(MOV)).

    # Type of Automated Station
    ("Type of Automated Station", _General(re.compile(r"^(?P<type>AO\d)\s+"))),
    ("Type of Automated Station?", _General(re.compile(r"^(?P<type>A[O0]\d[A-Z]?)\s+"))),  # undocumented. right?

    # Peak Wind (PK_WND_dddff(f)/(hh)mm)
    ("Peak Wind", _PeakWind()),

    # Wind Shift (WSHFT_(hh)mm)
    ("Wind Shift", _WindShift()),

    # Tower or Surface Visibility (TWR_VIS_vvvvv or SFC_VIS_vvvvv)
    ("Tower Visibility", _TowerOrSurfaceVisibility("TWR")),
    ("Surface Visibility", _TowerOrSurfaceVisibility("SFC")),

    # Variable Prevailing Visibility (VIS_vnvn vnvnVvxvxvx vxvx)
    ("Variable Prevailing Visibility", _VariablePrevailingVisibility()),

    # TODO: Sector Visibility (VIS_[DIR]_vvvvv){Plain Language]

    # TODO: Visibility At Second Location (VIS_vvvvv_[LOC])

    # Followings are sometimes merged, like "TS AND CB MOVD N-E"
    #
    #     Lightning (Frequency_LTG(type)_[LOC])
    #     Thunderstorm Location (TS_LOC_(MOV_DIR)) [Plain Language]
    #     Significant Cloud Types [Plain Language]
    #
    ("Lightning", _Lightning()),
    ("Thunderstorm Location", _ThunderstormLocation()),
    ("Significant Cloud Types", _SignificantCloudTypes()),

    # Beginning and Ending of Precipitation (w'w'B(hh)mmE(hh)mm)
    ("Beginning and Ending of Precipitation", _BeginningAndEndingOfPrecipitation()),

    # Beginning and Ending of Thunderstorms (TSB(hh)mmE(hh)mm)
    ("Beginning and Ending of Thunderstorms", _BeginningAndEndingOfThunderstorms()),

    # TODO: Hailstone Size (GR_[size])[Plain Language]

    # TODO: Virga (VIRGA_(DIR)) [Plain Language]

    # Variable Ceiling Height (CIG_hnhn hnVhxhxhx)
    ("Variable Ceiling Height", _VariableCeilingHeight()),

    # Obscurations (w'w'_[NsNsNs] hshshs) [Plain Language]
    ("Obscurations", _Obscurations()),

    # Variable Sky Condition (NsNsNs (hshshs)_V_ NsNsNs) [Plain Language]
    ("Variable Sky Condition", _VariableSkyCondition()),

    # TODO: Ceiling Height at Second Location (CIG_hhh_[LOC])

    # Pressure Rising or Falling Rapidly (PRESRR/PRESFR)
    ("Pressure Rising Rapidly", _General(re.compile(r"^PRESRR\s+"))),
    ("Pressure Falling Rapidly", _General(re.compile(r"^PRESFR\s+"))),

    # Sea-Level Pressure (SLPppp)
    ("Sea-Level Pressure", _SeaLevelPressure()),


    # TODO: Aircraft Mishap (ACFT_MSHP) [Plain Language]

    # TODO: No SPECI Reports Taken (NOSPECI) [Plain Language]

    # TODO: Snow Increasing Rapidly (SNINCR_[inches-hour/inches on ground])

    # TODO: Other Significant Information [Plain Language]

    # --- Additive Coded and Automated Maintenance Data
    
    # Precipitation
    #     Hourly Precipitation Amount (Prrrr)
    ("Hourly Precipitation Amount", _PrecipitationAmount("P")),

    #     3- and 6-hour Precipitation (6RRRR)
    ("3- and 6-hour Precipitation Amount", _PrecipitationAmount("6")),

    #     24-Hour Precipitation Amount (7R24R24R24R24)
    ("24-Hour Precipitation Amount", _PrecipitationAmount("7")),

    #    Snow Depth on Ground (4/sss)
    ("Snow Depth on Ground", _SnowDepthOnGround()),

    #    Water Equivalent of Snow on Ground (933RRR)
    ("Water Equivalent of Snow on Ground", _WaterEquivalentOfSnowOnGround()),

    # Cloud Types (8/CLCMCH)
    ("Cloud Types", _General(
            re.compile(r"^8/(?P<low>\d|/)(?P<middle>\d|/)(?P<high>\d|/)\s+"), False)),

    # Duration of Sunshine (98mmm)
    ("Duration of Sunshine", _DurationOfSunshine()),

    # Hourly Temperature and Dew Point (TsnT'T'T'snT'dT'dT'd)
    ("Hourly Temperature and Dew Point", _HourlyTemperatureAndDewPoint()),

    # 6-Hourly Maximum Temperature (1snTx TxTx)
    ("6-Hourly Maximum Temperature", _SixHourlyMaximumMinimumTemperature("1")),

    # 6-Hourly Minimum Temperature (2snTn TnTn)

    ("6-Hourly Minimum Temperature", _SixHourlyMaximumMinimumTemperature("2")),


    # 24-hour Maximum and Minimum Temperature (4sn TxTxTxsnTn TnTn)
    ("24-hour Maximum and Minimum Temperature", _TwentyfourHourMaximumMinimumTemperature()),

    # 3-Hourly Pressure Tendency (5appp)
    ("3-Hourly Pressure Tendency", _ThreeHourlyPressureTendency()),

    # Maintenance Indicator
    #    A maintenance indicator sign, $, shall be coded when an automated system
    #    detects that maintenance is needed on the system.
    ("Maintenance Indicator", _General(re.compile(r"^\$\s+"), False)),


    # -----------------------------
    #
    # EXTRAS.
    #   followings are no documentation as METAR spec, but actually is operating.
    #
    ("Wind at Specific Location or Distinctive Wind", _Wind()),

    ("Atmospheric Pressure (in other unit)", _Qnh()),

    # Density Altitude (DENSITY ALT hhhhFT)
    ("Density Altitude", _DensityAltitude()),

    # Color States
    ("Color States", _ColourState()),

    # QFE in Russian convention
    ("QFE (Russia)", _QFE_Russian()),

    # Amount of Accumulated Precipitation since the previous METAR (PPrrr) (Peru's convention)
    ("Accumulated Precipitation (Peru)",
     _AmountOfAccumulatedPrecipitationSinceThePrevious_Peru()),

    # Bird Hazard (BIRD HAZARD RWY rr[/rr]) (Peru's convention)
    ("Bird Hazard (Peru)", _BirdHazard_Peru()),

    # Cloud Cover in Japan convention (NsCCHsHsHs)
    ("Cloud Cover (Japan)", _CloudCover_Japan()),

    # Cloud cover or Obscuring Phenomena in Canada convention (CCNsCCNs...)
    ("Cloud Cover or Obscuring Phenomena (Canada)", _CloudCover_Canada()),


    #
    #
    # -----------------------------





    # --- FALLBACK
    ("__unk", _General(re.compile(r"(\S+)\s+"))),

    ]
