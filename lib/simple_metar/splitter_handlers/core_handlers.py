# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
import re
from ..abbreviations import (
    build_weather_codes_regexp,
    SKY_CONDITIONS,
    CLOUD_TYPES,
    )
from .common import _General, _Wind, _Qnh, _ColourState, _to_fracint


class _Visibility(_General):
    def __init__(self):
        super(_Visibility, self).__init__(
            re.compile(r"""^(
                       # WMO standard
                       (?P<distw>\d\d\d\d|////)
                       (?P<dir>[NSEW][EW]? | NDV)?
                       |
                       # WMO standard
                       (?P<cavok>CAVOK)
                       |
                       # NOAA FMH-1 1995
                       (?P<lessthan>M?)
                       (?P<distn>(\d+\s+)?\d+(/\d+)?)
                       (?P<units>SM|KM|M|U)
                       )\s+""",
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "cavok" in g:
            g["distance"] = g.pop("cavok")
        elif "distw" in g:
            if g["distw"] == '////':
                g["distance"] = g.pop("distw")
            else:
                g["distance"] = int(g.pop("distw"))
        elif "distn" in g:
            g["distance"] = _to_fracint(g.pop("distn"))
        if "lessthan" in g and g["lessthan"] == "M":
            g["lessthan"] = True
        return g
    

class _Rvr(_General):
    def __init__(self):
        # MISG=missing
        super(_Rvr, self).__init__(
            re.compile(r"""^(RVRNO |
                           RVR\sMISG |
                            R(?P<runway>0?\d\d[LRC]*)/
                             (?P<low_ltgt>(M|P)?)(?P<low>\d\d\d\d)
                           (V(?P<high_ltgt>(M|P)?)(?P<high>\d\d\d\d))?
                             (?P<unit>FT)?
                             (?P<tendency>[/NDU]?))\s+""",
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "low" in g:
            g["low"] = int(g["low"])
        if "high" in g:
            g["high"] = int(g["high"])
        return g

        
class _Weather(_General):
    def __init__(self):
        super(_Weather, self).__init__(
            build_weather_codes_regexp(
                True, r"^((", r")|/+)\s+"), True)


class _Sky(_General):
    def __init__(self):
        super(_Sky, self).__init__(
            re.compile(r"""^
                        (?P<cover>{}|///)
                        (?P<height>\d\d\d|///)?
                        (?P<cloud>({}|///))?\s+""".format(
                    "|".join(SKY_CONDITIONS.keys()),
                    "|".join(CLOUD_TYPES.keys())),
                       re.VERBOSE), True)

    def _reveal_groupdict(self, g):
        if "height" in g and g["height"] != '///':
            g["height"] = int(g["height"])
            g["height_unit"] = "100ft"
        return g

class _TempDewpt(_General):
    def __init__(self):
        super(_TempDewpt, self).__init__(
            re.compile(r"""^
                          # Both WMO 306 and NOAA FH-1 1995 don't mention those
                          # missing indicator, but original python-metar do handle...
                          ((?P<temp>(M|-)?\d+)|(?P<temp_missing>(//|XX|MM)))
                          /
                          ((?P<dewpt>(M|-)?\d+)|(?P<dewpt_missing>(//|XX|MM)))?
                          \s+""",
                       re.VERBOSE), False)

    def _reveal_groupdict(self, g):
        if "temp_missing" in g:
            g["temperature"] = g.pop("temp_missing")
        elif "temp" in g:
            g["temperature"] = int(re.sub("^M", "-", g.pop("temp")))
        if "dewpt_missing" in g:
            g["dewpoint"] = g.pop("dewpt_missing")
        elif "dewpt" in g:
            g["dewpoint"] = int(re.sub("^M", "-", g.pop("dewpt")))
        return g


# [(name, rgx, repeatable, cb), ...]
BASEINFOS = [
    #
    ("Wind", _Wind()),

    #
    ("Visibility", _Visibility()),

    #
    ("Runway Visual Range", _Rvr()),

    #
    ("Present Weather", _Weather()),

    #
    ("Sky Condition", _Sky()),

    #
    ("Temperature/Dew Point", _TempDewpt()),

    #
    ("Atmospheric Pressure", _Qnh()),

    # --- Supplementary Information Groups
    #
    ("Recent weather phenomena of operational significance", _General(
            build_weather_codes_regexp(False, r"^RE", r"\s+"), True)),

    # maybe format with TKOF/LDG is old-style...
    # maybe WS RWY runway is old-style... (now, WS R070L, etc.)
    ("Wind Shear", _General(
            re.compile(r"""^
                          (WS\s+)
                          (?P<tkof_ldg>((TKOF|LDG)\s+))?
                          (ALL\s+RWY|R(WY\s+)?(?P<runway>0?\d\d[LRC]*))
                          \s+""",
                       re.VERBOSE), True)),

    # Color States
    ("Color States", _ColourState()),

    #
    # maybe...runway state group has two styles, old style and new style.
    # almost all of ducumentation of this, explain only old style (without prefix `R' and
    # code 34R to 34+50=84, etc), but recently only new style is found (R + actual runway
    # name, like `R34L/').
    ("Runway State", _General(re.compile(r"""
                                (R(?P<runway>0?\d\d[LRC]*)|(?P<oldstyle_runwayname>\d\d))/?
                                ((?P<special>SNOCLO|CLRD(\d\d|//)) |
                                 (?P<deposit>(\d|/))
                                 (?P<extent>(\d|/))
                                 (?P<depth>(\d\d|//))
                                 (?P<friction>(\d\d|//)))\s+""",
                                         re.VERBOSE), True)),
]

# trend
TRENDINFOS = [
    ("Time", _General(re.compile(r"(?P<when>(FM|TL|AT))(?P<hour>\d\d)(?P<minute>\d\d)\s+"), False)),
    ("Wind", _Wind()),
    ("Visibility", _Visibility()),
    ("Weather", _Weather()),
    ("Sky", _Sky()),
    ("Color States", _ColourState()),
]
