# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import re

# all values of abbreviation tables here are consist of
#   (name, sub group, detail description if any)
# tuple.

# ==================================================================
#
# WEATHER CODES
# (WMO Code Table 4678)
#

WEATHER_CODES_INTENSITY_OR_PROXIMITY = {
    "–": ("Light", "1", ""),
    "": ("Moderate", "0", "(no qualifier)"),
    "+": ("Heavy", "1", ""),
    "VC": ("In the vicinity", "1", ""),
    }

WEATHER_CODES_DESCRIPTOR = {
    "MI": ("Shallow", "", ""),
    "BC": ("Patches", "", ""),
    "PR": ("Partial", "", ""),
    "DR": ("Drifting", "", ""),
    "BL": ("Blowing", "", ""),
    "SH": ("Shower(s)", "", ""),
    "TS": ("Thunderstorm", "", ""),
    "FZ": ("Freezing", "", ""),
    }

WEATHER_CODES_PRECIPITATION = {
    "DZ": ("Drizzle", "", ""),
    "RA": ("Rain", "", ""),
    "SN": ("Snow", "", ""),
    "SG": ("Snow Grains", "", ""),
    "IC": ("Ice Crystals", "", "(Vis ≤ 6 SM)"),
    "PL": ("Ice Pellets", "", ""),
    "GR": ("Hail", "", ""),
    "GS": ("Snow Pellets", "", ""),
    # AWOS: an automated weather observation system
    "UP": ("Unknown precipitation", "", "AWOS only"),
    }

WEATHER_CODES_OBSCURATION = {
    "BR": ("Mist", "", "(Vis ≥ 5/8 SM)"),
    "FG": ("Fog", "", "(Vis < 5/8 SM)"),
    "FU": ("Smoke", "", "(Vis ≤ 6 SM)"),
    "DU": ("Dust", "", "(Vis ≤ 6 SM)"),
    "SA": ("Sand", "", "(Vis ≤ 6 SM)"),
    "HZ": ("Haze", "", "(Vis ≤ 6 SM)"),
    "VA": ("Volcanic Ash", "", "(with any visibility)"),
    "PY": ("Spray", "", ""),
    }

WEATHER_CODES_PHENOMENA_OTHER = {
    "PO": ("Well-developed Dust/Sand Whirls", "1", "(Dust Devils)"),
    "SQ": ("Squalls", "1", ""),
    "FC": ("Funnel Cloud", "1", "Tornadoes and waterspouts shall always be coded as +FC"),
    # say "HEAVY FUNNEL CLOUD", or "TORNADO/WATERSPOUT" directly?
    "+FC": ("Tornado or Waterspout", "2", "Tornadoes and waterspouts shall always be coded as +FC"),
    "SS": ("Sandstorm", "1", "(Vis < 5/8 SM)(+SS Vis < 5/16 SM)"),
    "DS": ("Dust storm", "1", "(Vis < 5/8 SM)(+DS Vis < 5/16 SM)"),
    "NSW": ("no significant weather", "1", ""),
    }

#
# helper functions for building regular expressions
#
def build_weather_codes_regexp(with_int, pre, post):
    """
    with_int: with or without intensity_or_proximity.
    """
    core_parts = []
    if with_int:
        core_parts.append(
            "(?P<intensity_or_proximity>([+-]|VC))?")

    core_parts.append(
        "(?P<descriptor>({})+)?".format("|".join(
                WEATHER_CODES_DESCRIPTOR.keys())))
    core_parts.append("(?P<precipitation>({})*)".format("|".join(
                WEATHER_CODES_PRECIPITATION.keys())))
    core_parts.append("(?P<obscuration>{})?".format("|".join(
                WEATHER_CODES_OBSCURATION.keys())))
    core_parts.append("(?P<other>{})?".format("|".join(
                [k for k, v in WEATHER_CODES_PHENOMENA_OTHER.items()
                 if v[1] == "1"])))

    return re.compile(pre + "".join(core_parts) + post, re.VERBOSE)

#
#
#
# ==================================================================

# ==================================================================
#
# SKY CONDITIONS
#
SKY_CONDITIONS = {
    "SKC": ("Sky Clear", "std", "no cloud present"),
    "FEW": ("Few", "std", "less than 1/8 to 2/8 summation amount"),
    "SCT": ("Scattered", "std", "3/8 to 4/8 summation amount"),
    "BKN": ("Broken", "std", "5/8 to less than 8/8 summation amount"),
    "OVC": ("Overcast", "std", "8/8 summation amount"),
    "CLR": ("Clear", "std", "clear below 25 000 ft as interpreted by an AWOS"),
    "VV": ("Vertical Visibility", "std", "8/8 summation amount"),

    "NSC": ("No Significant Cloud", "std", ""),
    "NCD": ("No Cloud Detected", "std", ""),

    # maybe those occur at non AWOS stations
    "SCK": ("Sky Clear?", "mispell", ""),
    "0VC": ("Overcast", "mispell", ""),
    }
#
#
#
# ==================================================================

# ==================================================================
#
# CLOUD TYPES
#
CLOUD_TYPES = {
    # not considered to be significant to aviation
    "CI": ("Cirrus", "3", ""),
    "CS": ("Cirrostratus", "3", ""),
    "CC": ("Cirrocumulus", "3", ""),

    #
    "AC": ("Alto Cumulus", "2", ""),
    "AS": ("Alto Stratus", "2", ""),
    "NS": ("Nimbo Stratus", "2", ""),
    "SC": ("Stratocumulus", "2", ""),
    "ST": ("Stratus", "2", ""),
    "CU": ("Cumulus", "2", ""),

    "CF": ("Cumulus Fractus", "2", ""),
    "SL": ("Standing Lenticular Cloud", "2", ""),
    "SF": ("Stratus Fractus", "2", ""),

    # Significant
    "ACC": ("Altocumulus Castellanus", "1", ""),
    "CB": ("Cumulonimbus", "1", ""),
    "TCU": ("Towering Cumulus", "1", ""),
    }

#
#
# ==================================================================
