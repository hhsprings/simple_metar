# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# =======================================================================
#
# This script is almost all based on
# python-metar <https://github.com/phobson/python-metar> by Tom Pollard,
# but my version is very simple to avoid depend on many third party
# libraries.
#
# Explanations from python-metar:
# --------------------------------------------------------------
#  US conventions for METAR/SPECI reports are described in chapter 12 of
#  the Federal Meteorological Handbook No.1. (FMH-1 1995), issued by NOAA.
#  See <http://metar.noaa.gov/>
#
#  International conventions for the METAR and SPECI codes are specified in
#  the WMO Manual on Codes, vol I.1, Part A (WMO-306 I.i.A).
#
#  This module handles a reports that follow the US conventions, as well
#  the more general encodings in the WMO spec.  Other regional conventions
#  are not supported at present.
#
#  The current METAR report for a given station is available at the URL
#  http://weather.noaa.gov/pub/data/observations/metar/stations/<station>.TXT
#  where <station> is the four-letter ICAO station code.
#
#  The METAR reports for all reporting stations for any "cycle" (i.e., hour)
#  in the last 24 hours is available in a single file at the URL
#  http://weather.noaa.gov/pub/data/observations/metar/cycles/<cycle>Z.TXT
#  where <cycle> is a 2-digit cycle number (e.g., "00", "05" or "23").
# --------------------------------------------------------------
#
# =======================================================================
#
#
#
#
from __future__ import absolute_import
import re
from .countries_map import countries_from_station
from .splitter_handlers.core_handlers import (BASEINFOS, TRENDINFOS)
from .splitter_handlers.remark_handlers import (REMARKINFOS)


# ---------------------------------------------------------------------------
#
# Module globals
#

# regular expressions for header informations
_HEADERS_RGXES = [
    ("Type of Report", re.compile(r"^(?P<type>METAR|SPECI)\s+")),
    ("Station Identifier", re.compile(r"^(?P<station>[A-Z][A-Z0-9]{3})\s+")),

    # maybe in manual station, space would be missing.
    ("Date and Time of Report", re.compile(r"""^(?P<time>\d\d\d\d\d\dZ?)\s?""")),

    ("Report Modifier", re.compile(r"^(?P<mod>AUTO|FINO|NIL|TEST|CORR?|RTD|CC[A-G])\s+")),
]

# trend
_TREND_RGX = re.compile(r"^(?P<trend>TEMPO|BECMG|FCST|NOSIG)\s*")

# remarks
_REMARK_RGX = re.compile(r"^(RMKS?|NOSPECI|NOSIG)\s*")


# ------------------------------------------------

def split(code):
    code += " "

    # headers
    result = {}
    result["header"] = {}
    for n, rgx in _HEADERS_RGXES:
        m = rgx.match(code)
        if m:
            result["header"][n] = m.group(1)
            code = code[m.end():]

    station = dict(result["header"])["Station Identifier"]

    # base
    result["base"] = []
    for n, obj in BASEINFOS:
        gall, rvldict, code = obj.match(code)
        while gall:
            result["base"].append((n, [gall, rvldict]))
            if not obj.repeatable:
                break
            gall, rvldict, code = obj.match(code)

    # trend
    m = _TREND_RGX.match(code)
    if m:
        result["trend"] = (m.group().rstrip(), [])
        code = code[m.end():]
        for n, obj in TRENDINFOS:
            gall, rvldict, code = obj.match(code)
            while gall:
                result["trend"][1].append((n, [gall, rvldict]))
                if not obj.repeatable:
                    break
                gall, rvldict, code = obj.match(code)

    # remarks
    m = _REMARK_RGX.match(code)
    if m:
        remark_type = m.group().rstrip()
        result["remark"] = (remark_type, [])

        code = code[m.end():]
        countries = countries_from_station(station)

        last_n = None
        while code:
            for n, obj in REMARKINFOS:
                if obj.convension(countries) == "not using":
                    continue
                gall, rvldict, code = obj.match(code)
                if gall:
                    r = [gall, rvldict]
                    if n == last_n and n == "__unk":
                        result["remark"][1][-1][1][0] += " " + r[0]
                    else:
                        result["remark"][1].append((n, r))
                    last_n = n
                    break

    return result
