# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from __future__ import absolute_import
import os
import csv
import pickle
try:
    from cStringIO import StringIO  # CPython 2.7
except ImportError:
    from StringIO import StringIO  # Python 3.x or not CPython
from .config import config


# --- module globals
#
_MAP_STCODE_PREFIX_TO_COUNTRY = {}
# ---


# --- public interfaces
#
def get_prefix_map():
    global _MAP_STCODE_PREFIX_TO_COUNTRY
    cache_dir = config.get_cache_dir()

    if not _MAP_STCODE_PREFIX_TO_COUNTRY:
        pf = os.path.join(cache_dir, "_MAP_STCODE_PREFIX_TO_COUNTRY.pickle")
        if os.path.exists(pf):
            _MAP_STCODE_PREFIX_TO_COUNTRY = pickle.load(open(pf, "rb"))
        else:
            path = os.path.join(os.path.dirname(__file__), "data", "icao_code_prefixes.txt")
            contents = open(path).read()
            reader = csv.reader(StringIO(contents), delimiter="/")
            for row in reader:
                country, prefixes = row[0], row[1].split(",")
                for prefix in prefixes:
                    if prefix not in _MAP_STCODE_PREFIX_TO_COUNTRY:
                        _MAP_STCODE_PREFIX_TO_COUNTRY[prefix] = []
                    _MAP_STCODE_PREFIX_TO_COUNTRY[prefix].append(country)
            pickle.dump(_MAP_STCODE_PREFIX_TO_COUNTRY, open(pf, "wb"), protocol=-1)

    return _MAP_STCODE_PREFIX_TO_COUNTRY


def countries_from_station(station):
    prefixes = get_prefix_map()
    return prefixes.get(
        station[:3],
        prefixes.get(
            station[:2],
            prefixes.get(
                station[:1], [])))
