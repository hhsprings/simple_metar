# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import sys
import os
import re
import json
from simple_metar.splitter import split
from simple_metar.noaa_metar_services import (
    get_current_by_single_station,
    search_stations_by_boundingbox,
    search_stations_by_stationid)
if sys.platform == "win32":
    import os, msvcrt
    msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
    msvcrt.setmode(sys.stderr.fileno(), os.O_BINARY)

# ------------------------------
ctype = os.environ.get('CONTENT_TYPE', "")
clength = -1
if os.environ.get('CONTENT_LENGTH'):
    clength = int(os.environ.get('CONTENT_LENGTH'))
postdata = sys.stdin.read(clength)
form = {}

try:
    if not ctype or re.search(r"\bapplication/json\b", ctype):
        form = json.loads(postdata)
except ValueError:
    pass

result = []

if form["command"] == "search_stations":
    by = form["params"]["by"]
    if by[0] == "boundingbox":
        for res in search_stations_by_boundingbox(by[1], by[2], by[3], by[4]):
            sid, (name, lon, lat) = res
            result.append({"station": sid, "name": name, "lon": lon, "lat": lat})
        sys.stdout.write("Status: 200 OK\n")
        sys.stdout.write("Content-Type: application/json\n")
        sys.stdout.write("\n")
        sys.stdout.flush()
        sys.stdout.write(json.dumps({"result": result}))
        sys.stdout.flush()
        sys.exit(0)
    elif by[0] == "station_id":
        for sid in by[1:]:
            res = search_stations_by_stationid(sid)
            name, lon, lat = "N/F", float('nan'), float('nan')
            if res:
                name, lon, lat = res
            result.append({"station": sid, "name": name, "lon": lon, "lat": lat})
        sys.stdout.write("Status: 200 OK\n")
        sys.stdout.write("Content-Type: application/json\n")
        sys.stdout.write("\n")
        sys.stdout.flush()
        sys.stdout.write(json.dumps({"result": result}))
        sys.stdout.flush()
        sys.exit(0)

elif form["command"] == "get_METAR":
    station_id = form["params"][0]
    for dt, code in get_current_by_single_station(station_id):
        res = split(code)
        result.append((str(dt), code, res))

    sys.stdout.write("Status: 200 OK\n")
    sys.stdout.write("Content-Type: application/json\n")
    sys.stdout.write("\n")
    sys.stdout.flush()
    sys.stdout.write(json.dumps({"result": result}))
    sys.stdout.flush()
    sys.exit(0)
