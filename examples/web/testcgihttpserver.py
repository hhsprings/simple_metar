# -*- coding: utf-8 -*-
import copy
import os
import sys
import select


try:
    import urllib.parse  # Python 3.x
    def unquote(s):
        return urllib.parse.unquote(s)
    import http.server as cgihttpserver_mod
    _PY27 = False

except ImportError:
    import urllib
    def unquote(s):
        return urllib.unquote(s)
    import CGIHTTPServer as cgihttpserver_mod # Python 2.7
    _PY27 = True


nobody_uid = cgihttpserver_mod.nobody_uid


def _run_cgi_process(self, env, script, scriptfile, query, decoded_query, length):
    if self.have_fork:
        # Unix -- fork as we should
        args = [script]
        if '=' not in decoded_query:
            args.append(decoded_query)
        nobody = nobody_uid()
        self.wfile.flush() # Always flush before forking
        pid = os.fork()
        if pid != 0:
            # Parent
            pid, sts = os.waitpid(pid, 0)
            # throw away additional data [see bug #427345]
            while select.select([self.rfile], [], [], 0)[0]:
                if not self.rfile.read(1):
                    break
            if sts:
                self.log_error("CGI script exit status %#x", sts)
            return
        # Child
        try:
            try:
                os.setuid(nobody)
            except (os.error, OSError):
                pass
            os.dup2(self.rfile.fileno(), 0)
            os.dup2(self.wfile.fileno(), 1)
            os.execve(scriptfile, args, env)
        except:
            self.server.handle_error(self.request, self.client_address)
            os._exit(127)

    else:
        # Non-Unix -- use subprocess
        import subprocess
        cmdline = [scriptfile]
        if self.is_python(scriptfile):
            interp = sys.executable
            if interp.lower().endswith("w.exe"):
                # On Windows, use python.exe, not pythonw.exe
                interp = interp[:-5] + interp[-4:]
            cmdline = [interp, '-u'] + cmdline
        elif os.path.splitext(scriptfile)[1] not in (".exe", ".bat"):
            # with MSYS or Cygwin (ba-)sh
            cmdline.insert(0, "sh")
        if '=' not in query:
            cmdline.append(query)
        self.log_message("command: %s", subprocess.list2cmdline(cmdline))
        try:
            nbytes = int(length)
        except (TypeError, ValueError):
            nbytes = 0
        p = subprocess.Popen(cmdline,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             env = env
                             )
        if self.command.lower() == "post" and nbytes > 0:
            data = self.rfile.read(nbytes)
        else:
            data = None
        # throw away additional data [see bug #427345]
        while select.select([self.rfile._sock], [], [], 0)[0]:
            if not self.rfile._sock.recv(1):
                break
        stdout, stderr = p.communicate(data)
        self.wfile.write(stdout)
        if stderr:
            self.log_error('%s', stderr)
        p.stderr.close()
        p.stdout.close()
        status = p.returncode
        if status:
            self.log_error("CGI script exit status %#x", status)
        else:
            self.log_message("CGI script exited OK")


def run_cgi(self):
    """Execute a CGI script."""
    dir, rest = self.cgi_info
    path = dir + '/' + rest
    i = path.find('/', len(dir)+1)
    while i >= 0:
        nextdir = path[:i]
        nextrest = path[i+1:]

        scriptdir = self.translate_path(nextdir)
        if os.path.isdir(scriptdir):
            dir, rest = nextdir, nextrest
            i = path.find('/', len(dir)+1)
        else:
            break

    # find an explicit query string, if present.
    i = rest.rfind('?')
    if i >= 0:
        rest, query = rest[:i], rest[i+1:]
    else:
        query = ''

    # dissect the part after the directory name into a script name &
    # a possible additional path, to be stored in PATH_INFO.
    i = rest.find('/')
    if i >= 0:
        script, rest = rest[:i], rest[i:]
    else:
        script, rest = rest, ''

    scriptname = dir + '/' + script
    scriptfile = self.translate_path(scriptname)
    if not os.path.exists(scriptfile):
        self.send_error(404, "No such CGI script (%r)" % scriptname)
        return
    if not os.path.isfile(scriptfile):
        self.send_error(403, "CGI script is not a plain file (%r)" %
                        scriptname)
        return
    ispy = self.is_python(scriptname)
    if self.have_fork or not ispy:
        if not self.is_executable(scriptfile):
            self.send_error(403, "CGI script is not executable (%r)" %
                            scriptname)
            return

    # Reference: http://hoohoo.ncsa.uiuc.edu/cgi/env.html
    # XXX Much of the following could be prepared ahead of time!
    env = copy.deepcopy(os.environ)
    env['SERVER_SOFTWARE'] = self.version_string()
    env['SERVER_NAME'] = self.server.server_name
    env['GATEWAY_INTERFACE'] = 'CGI/1.1'
    env['SERVER_PROTOCOL'] = self.protocol_version
    env['SERVER_PORT'] = str(self.server.server_port)
    env['REQUEST_METHOD'] = self.command
    uqrest = unquote(rest)
    env['PATH_INFO'] = uqrest
    env['PATH_TRANSLATED'] = self.translate_path(uqrest)
    env['SCRIPT_NAME'] = scriptname
    if query:
        env['QUERY_STRING'] = query
    env['REMOTE_ADDR'] = self.client_address[0]
    authorization = self.headers.get("authorization")
    if authorization:
        authorization = authorization.split()
        if len(authorization) == 2:
            import base64, binascii
            if not hasattr(base64, 'decodebytes'):
                # Python 2.7
                base64.decodebytes = base64.decodestring
            env['AUTH_TYPE'] = authorization[0]
            if authorization[0].lower() == "basic":
                try:
                    authorization = authorization[1].encode('ascii')
                    authorization = base64.decodebytes(authorization).\
                                    decode('ascii')
                except (binascii.Error, UnicodeError):
                    pass
                else:
                    authorization = authorization.split(':')
                    if len(authorization) == 2:
                        env['REMOTE_USER'] = authorization[0]
    # XXX REMOTE_IDENT
    if self.headers.get('content-type') is None:
        env['CONTENT_TYPE'] = self.headers.get_content_type()
    else:
        env['CONTENT_TYPE'] = self.headers['content-type']
    length = self.headers.get('content-length')
    if length:
        env['CONTENT_LENGTH'] = length
    referer = self.headers.get('referer')
    if referer:
        env['HTTP_REFERER'] = referer
    accept = self.headers.get_all('accept', ())
    env['HTTP_ACCEPT'] = ','.join(accept)
    # ---------------------------------------- ADD BEGIN
    accept_encoding = self.headers.get_all('accept-encoding', ())
    env['HTTP_ACCEPT_ENCODING'] = ','.join(accept_encoding)
    accept_charset = self.headers.get_all('accept-charset', ())
    env['HTTP_ACCEPT_CHARSET'] = ','.join(accept_charset)
    accept_language = self.headers.get_all('accept-language', ())
    env['HTTP_ACCEPT_LANGUAGE'] = ','.join(accept_language)
    # ---------------------------------------- ADD END
    ua = self.headers.get('user-agent')
    if ua:
        env['HTTP_USER_AGENT'] = ua
    co = filter(None, self.headers.get_all('cookie', []))
    cookie_str = ', '.join(co)
    if cookie_str:
        env['HTTP_COOKIE'] = cookie_str
    # XXX Other HTTP_* headers
    # Since we're setting the env in the parent, provide empty
    # values to override previously set values
    for k in ('QUERY_STRING', 'REMOTE_HOST', 'CONTENT_LENGTH',
              'HTTP_USER_AGENT', 'HTTP_COOKIE', 'HTTP_REFERER'):
        env.setdefault(k, "")

    self.send_response(200, "Script output follows")
    self.flush_headers()

    decoded_query = query.replace('+', ' ')

    self._run_cgi_process(env, script, scriptfile, query, decoded_query, length)


if _PY27:
    import email.message
    import mimetools

    def _splitparam(param):
        # Split header parameters.  BAW: this may be too simple.  It isn't
        # strictly RFC 2045 (section 5.1) compliant, but it catches most headers
        # found in the wild.  We may eventually need a full fledged parser.
        # RDM: we might have a Header here; for now just stringify it.
        a, sep, b = str(param).partition(';')
        if not sep:
            return a.strip(), None
        return a.strip(), b.strip()

    class _Message(mimetools.Message):
        def get(self, name, failobj=None):
            return self.getheader(name, failobj)

        def get_all(self, name, fallback=()):
            return self.getheaders(name)

        def __getitem__(self, name):
            return self.get(name)

        def get_content_type(self):
            """Return the message's content type.
    
            The returned string is coerced to lower case of the form
            `maintype/subtype'.  If there was no Content-Type header in the
            message, the default type as given by get_default_type() will be
            returned.  Since according to RFC 2045, messages always have a default
            type this will always return a value.
    
            RFC 2045 defines a message's default type to be text/plain unless it
            appears inside a multipart/digest container, in which case it would be
            message/rfc822.
            """
            missing = object()
            value = self.get('content-type', missing)
            if value is missing:
                # This should have no parameters
                return 'text/plain'
            ctype = _splitparam(value)[0].lower()
            # RFC 2045, section 5.2 says if its invalid, use text/plain
            if ctype.count('/') != 1:
                return 'text/plain'
            return ctype


    class LocalPatchedCGIHTTPRequestHandler(cgihttpserver_mod.CGIHTTPRequestHandler):

        def __init__(self, request, server_address, RequestHandlerClass):
            self.MessageClass = _Message
            cgihttpserver_mod.CGIHTTPRequestHandler.__init__(
                self, request, server_address, RequestHandlerClass)

        def is_executable(self, path):
            """Test whether argument path is an executable file."""
            return os.access(path, os.X_OK)

        def flush_headers(self):
            pass


    LocalPatchedCGIHTTPRequestHandler._run_cgi_process = _run_cgi_process
    LocalPatchedCGIHTTPRequestHandler.run_cgi = run_cgi


else:  # Python 3.x
    LocalPatchedCGIHTTPRequestHandler = cgihttpserver_mod.CGIHTTPRequestHandler
    LocalPatchedCGIHTTPRequestHandler._run_cgi_process = _run_cgi_process
    LocalPatchedCGIHTTPRequestHandler.run_cgi = run_cgi


def test(**kw):
    """Test the HTTP request handler class.

    This runs an HTTP server on port 8000 (or the first command line
    argument).

    """
    kw["HandlerClass"] = LocalPatchedCGIHTTPRequestHandler
    cgihttpserver_mod.test(**kw)


if __name__ == "__main__":
    test()
