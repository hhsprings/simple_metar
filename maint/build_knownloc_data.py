# -*- coding: utf-8 -*-
#
# Copyright (c) 2016, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from os import path
import csv
import pickle
# pytree exists in https://code.google.com/archive/p/pyrtree/,
# i.e., that is slightly old (project is dead), but it is written
# as pure Python, so it is proper for me. (Though it doesn't have
# nearest neighbour lookup, but it's not difficult to write it.)
from pyrtree import RTree, Rect


_targdir = path.join(path.dirname(path.abspath(__file__)), "../lib/simple_metar/data")
rt = RTree()
baseinfos = {}
#
reader = csv.reader(open("knownloc_data_raw.csv"))
baseinfos = {}
for code, name, lon, lat in reader:
    if code == "code":
        continue
    lon, lat = float(lon), float(lat)
    baseinfos[code] = (name, lon, lat)
    # Choise of the value 1/60 has no meaningful reason...
    mbr = Rect(lon - 1/60., lat - 1/60., lon + 1/60., lat + 1/60.)
    rt.insert(code, mbr)
#
pickle.dump({"index": rt, "infos": baseinfos},
            open(path.join(_targdir, "knownloc_data.pickle"), "wb"), protocol=-1)
