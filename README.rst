simple_metar
============
This is a very simple python package for interpreting METAR and SPECI coded weather reports.


Installing
----------
If you don't have httplib2, you will need to install it:

.. code-block:: console

    me@host: ~$ pip install httplib2

And then:

.. code-block:: console

    me@host: ~$ cd simple_metar
    me@host: simple_metar$ python setup.py install

Examples
--------
.. code-block:: pycon

    >>> from simple_metar.splitter import split
    >>> from simple_metar.noaa_metar_services import get_current_by_single_station
    >>> for dt, code in get_current_by_single_station("RJTT"):
    ...     print(split(code))
    ...
    {'trend': ('NOSIG', []), 'header': {'Date and Time of Report': '262000Z', 'Station Identifier': 'RJTT'}, 'base': [('Wind', ('07004KT', {'units': 'KT', 'direction': 70, 'speed': 4})), ('Visibility', ('9999', {'distance': 9999})), ('Sky Condition', ('FEW010', {'cover': 'FEW', 'height_unit': '100ft', 'height': 10})), ('Temperature/Dew Point', ('26/25', {'dewpoint': 25, 'temperature': 26})), ('Atmospheric Pressure', ('Q1004', {'pressure': 1004, 'unit': 'hPa'}))]}
    >>> 
    >>> for dt, code in get_current_by_single_station("RJTT"):
    ...     result = split(code)
    ...     # print only originals of base
    ...     print(" ".join([r[1][0] for r in result["base"]]))
    ...
    07004KT 9999 FEW010 26/25 Q1004
    >>> 
    >>> for dt, code in get_current_by_single_station("RJTT"):
    ...     result = split(code)
    ...     # print only parsed of base
    ...     print(" ".join([str(r[1][1]) for r in result["base"]]))
    ...
    {'units': 'KT', 'direction': 70, 'speed': 4} {'distance': 9999} {'cover': 'FEW', 'height_unit': '100ft', 'height': 10} {'dewpoint': 25, 'temperature': 26} {'pressure': 1004, 'unit': 'hPa'}
    >>> 

Following example is to render NOAA observations of current cycle to simple html:

.. code-block:: python

    from simple_metar.splitter import split
    from simple_metar.noaa_metar_services import get_current_cycle
    
    print("<html><body>")
    for dt, code in get_current_cycle():
    
        res = split(code)
        print("""\
    <h2>{station} at {time}</h2>
    """.format(**res['header']))
        print("""\
    <table border="1">""")
        for row in res['base']:
            print("""\
    <tr><td>{}</td><td>{}</td><td>{}</td></tr>""".format(row[0], row[1][0], row[1][1]))
        print("""\
    </table>
    """)
    
        if 'trend' in res:
            trend_type, trend_data = res['trend']
            print("<h3>{}</h3>".format(trend_type))
            print("""\
    <table border="1">""")
            for row in trend_data:
                print("""\
    <tr><td>{}</td><td>{}</td><td>{}</td></tr>""".format(row[0], row[1][0], row[1][1]))
            print("""\
    </table>
    """)
    
    
        if 'remark' in res:
            remark_type, remark_data = res['remark']
            print("<h3>{}</h3>".format(remark_type))
            print("""\
    <table border="1">""")
            for row in remark_data:
                print("""\
    <tr><td>{}</td><td>{}</td><td>{}</td></tr>""".format(row[0], row[1][0], row[1][1]))
            print("""\
    </table>
    """)
    
    print("</body></html>")
