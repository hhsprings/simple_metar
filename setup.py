# -*- coding: utf-8 -*-
from distutils.core import setup
import os
import glob


datafiles = glob.glob("lib/simple_metar/data/*")
datafiles = [os.path.join('data', os.path.basename(f)) for f in datafiles]
package_data = {'simple_metar': datafiles}

setup(
    name="simple_metar",
    version="0.0.0.1",
    description="a python package for interpreting METAR and SPECI coded weather reports.",
    packages=['simple_metar', 'simple_metar/splitter_handlers'],
    package_dir={
        'simple_metar': 'lib/simple_metar',
        'simple_metar/splitter_handlers': 'lib/simple_metar/splitter_handlers',
        },
    package_data=package_data,
)
